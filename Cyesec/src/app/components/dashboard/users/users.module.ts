import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { UsersComponent } from './users.component';
import { UserDetailsComponent } from './user-details/user-details.component';
import { AddUserComponent } from './add-user/add-user.component';
import { UsersResolverService } from './users-resolver.service';
import { UsersRoutingModule } from './users-routing.module';

import * as fromUsers from './store/users.reducer';
import { UsersEffects } from './store/users.effects';

@NgModule({
  declarations: [
    UsersComponent,
    UserDetailsComponent,
    AddUserComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    UsersRoutingModule,
    StoreModule.forFeature('users', fromUsers.usersReducer),
    EffectsModule.forFeature([UsersEffects])
  ],
  providers: [UsersResolverService]
})
export class UsersModule {}
