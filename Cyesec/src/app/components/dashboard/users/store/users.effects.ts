import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Actions, ofType, createEffect } from '@ngrx/effects';

import { exhaustMap, map, tap, switchMap } from 'rxjs/operators';

import * as UsersActions from './users.actions';
import { User } from '../../../../models/user.model';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router } from '@angular/router';


interface ApiUsersResponse {
  page: number;
  per_page: number;
  total: number;
  total_pages: number;
  data: User[];
}

@Injectable()
export class UsersEffects {

  fetchUsers$ = createEffect(() =>
    this.actions$.pipe(
      ofType(UsersActions.fetchUsers),
      exhaustMap(() => {
        return this.http.get<ApiUsersResponse>('https://reqres.in/api/users?page=2');
      }),
      map(apiRes => {
        return UsersActions.setUsers({ users: apiRes.data });
      })
    )
  );

  createUser$ = createEffect(() =>
    this.actions$.pipe(
      ofType(UsersActions.addUser),
      switchMap(action => {
        return this.http.post<{
          name: string,
          job: string,
          id: string,
          createdAt: string
        }>('https://reqres.in/api/users', action.user).pipe(
          tap(response => {
            this.flashMessage.show('RESPONSE: ' + JSON.stringify(response), { cssClass: 'alert-success', timeout: 4000 });
          }),
          map(response => {
            const user = new User(
              +response.id,
              response.name,
              'https://www.mnleadership.org/wp-content/uploads/2017/02/Anonymous-Avatar.png');
            return UsersActions.onUserAdded({ user });
          })
        );
      }),

    )
  );

  onUserAdded$ = createEffect(() =>
    this.actions$.pipe(
      ofType(UsersActions.onUserAdded),
      tap(() => {
        this.router.navigate(['../']);
      })
    ), { dispatch: false }
  );

  constructor(
    private actions$: Actions,
    private http: HttpClient,
    private flashMessage: FlashMessagesService,
    private router: Router
  ) { }

}
