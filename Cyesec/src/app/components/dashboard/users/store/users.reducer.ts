import { Action, createReducer, on } from '@ngrx/store';

import * as UsersActions from './users.actions';

import { User } from 'src/app/models/user.model';

export interface State {
  users: User[];
  selectedUser: User;
}

const initialState: State = {
  users: null,
  selectedUser: null
};

export function usersReducer(usersState: State | undefined, usersAction: Action) {
  return createReducer(
    initialState,
    on(UsersActions.setUsers,
      (state, action) => ({
        ...state,
        users: action.users
      })
    ),
    on(UsersActions.selectUser,
      (state, action) => ({
        ...state,
        selectedUser: action.selectedUser
      })
    ),
    on(UsersActions.onUserAdded,
      (state, action) => ({
        ...state,
        users: state.users.concat({ ...action.user })
      })
    )
  )(usersState, usersAction);
}
