import { createAction, props } from '@ngrx/store';

import { User } from 'src/app/models/user.model';

export const setUsers = createAction(
  '[Users] Set Users',
  props<{ users: User[]}>()
);

export const fetchUsers = createAction(
  '[Users] Fetch Users'
);

export const selectUser = createAction(
  '[Users] Select User',
  props<{ selectedUser: User }>()
);

export const addUser = createAction(
  '[Users] Add User',
  props<{ user: { name: string, job: string } }>()
);

export const onUserAdded = createAction(
  '[Users] On User Added',
  props<{ user: User }>()
);

