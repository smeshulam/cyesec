import { Component, OnInit, OnDestroy } from '@angular/core';

import { Store } from '@ngrx/store';

import * as fromUsers from '../users/store/index';

import * as UsersActions from './store/users.actions';
import { Subscription } from 'rxjs';
import { User } from '../../../models/user.model';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit, OnDestroy {
  users: User[];
  selectedUser: User;
  private subscription: Subscription;

  constructor(private store: Store<fromUsers.UsersState>) { }

  ngOnInit() {
    this.subscription = this.store.select('users').subscribe(usersState => {
      this.users = usersState.users;
      this.selectedUser = usersState.selectedUser;
      if (!this.users) {
        this.store.dispatch(UsersActions.fetchUsers());
      }
    });
  }

  onSelectUser(selectedUser: User) {
    this.store.dispatch(UsersActions.selectUser({ selectedUser }));
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
