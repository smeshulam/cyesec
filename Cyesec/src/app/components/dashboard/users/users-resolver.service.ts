import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Store } from '@ngrx/store';
import { Actions, ofType } from '@ngrx/effects';

import { map, take, switchMap } from 'rxjs/operators';

import { of } from 'rxjs';

import * as fromUsers from './store/index';

import * as UsersActions from './store/users.actions';
import { User } from 'src/app/models/user.model';

@Injectable()
export class UsersResolverService implements Resolve<{ users: User[] }> {

  constructor(
    private store: Store<fromUsers.UsersState>,
    private actions$: Actions
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.store.select('users').pipe(
      take(1),
      map(usersState => {
        return usersState.users;
      }),
      switchMap(users => {
        if (!users) {
          this.store.dispatch(UsersActions.fetchUsers());
          return this.actions$.pipe(
            ofType(UsersActions.setUsers),
            take(1)
          );
        } else {
          return of ({ users });
        }
      })
    );
  }
}
