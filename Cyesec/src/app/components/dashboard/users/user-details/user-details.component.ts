import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import { User } from 'src/app/models/user.model';

import * as fromUsers from '../store/index';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent implements OnInit, OnDestroy {
  selectedUser: User;
  private subscription: Subscription;

  constructor(private store: Store<fromUsers.UsersState>) { }

  ngOnInit() {
    this.subscription = this.store.select('users').subscribe(usersState => {
      this.selectedUser = usersState.selectedUser;
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
