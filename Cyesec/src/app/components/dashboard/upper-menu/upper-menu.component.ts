import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';

import { faGlobeAfrica, faSignOutAlt } from '@fortawesome/free-solid-svg-icons';

import * as fromApp from '../../../store/app.reducer';
import * as AuthActions from '../../../auth/auth.actions';

@Component({
  selector: 'app-upper-menu',
  templateUrl: './upper-menu.component.html',
  styleUrls: ['./upper-menu.component.css']
})
export class UpperMenuComponent implements OnInit {
  // Icons.
  faGlobeAfrica = faGlobeAfrica;
  faSignOutAlt = faSignOutAlt;

  constructor(private store: Store<fromApp.AppState>) { }

  ngOnInit() {
  }

  onLogout() {
    this.store.dispatch(AuthActions.logout());
  }

}
