import { Component, OnInit } from '@angular/core';

import { faNewspaper, faFile, faTachometerAlt, faUsers, faInfoCircle, faLifeRing, faCog } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  // Icons.
  faTachometerAlt = faTachometerAlt;
  faFile = faFile;
  faNewspaper = faNewspaper;
  faUsers = faUsers;
  faInfoCircle = faInfoCircle;
  faLifeRing = faLifeRing;
  faCog = faCog;

  constructor() { }

  ngOnInit() {
  }

}
