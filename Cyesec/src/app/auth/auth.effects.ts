import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { Router } from '@angular/router';

import { map, tap, catchError, switchMap } from 'rxjs/operators';
import { of } from 'rxjs';

import * as AuthActions from './auth.actions';
import * as fromApp from '../store/app.reducer';
import * as UsersActions from '../components/dashboard/users/store/users.actions';

import { FlashMessagesService } from 'angular2-flash-messages';
import { Store } from '@ngrx/store';

const handleAuthentication = (token: string, redirect: boolean, id?: number) => {
  localStorage.setItem('token', token);
  return AuthActions.authenticateSuccess({ token, redirect, id });
};

const handleError = (errorMessage: string) => {
  return of(AuthActions.authenticateFail({ errorMessage }));
};

@Injectable()
export class AuthEffects {

  register$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.register),
      switchMap(action => {
        return this.http.post<{ token: string, id: number }>('https://reqres.in/api/register',
          { email: action.email, password: action.password })
          .pipe(
            map(response => {
              return handleAuthentication(response.token, true, response.id);
            }),
            catchError((error: HttpErrorResponse) => {
              return handleError(error.error.error);
            })
          );
      }),
    )
  );

  login$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.login),
      switchMap(action => {
        return this.http.post<{ token: string }>('https://reqres.in/api/login',
          { email: action.email, password: action.password })
          .pipe(
            map(response => {
              return handleAuthentication(response.token, true);
            }),
            catchError((error: HttpErrorResponse) => {
              return handleError(error.error.error);
            })
          );
      })
    )
  );

  authRedirect$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.authenticateSuccess),
      tap(action => {
        if (action.redirect) {
          this.router.navigate(['/']);
        }
      })
    ), { dispatch: false }
  );

  authFail$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.authenticateFail),
      tap(action => {
        this.flashMessage.show(action.errorMessage, { cssClass: 'alert-danger', timeout: 4000 });
      })
    ), { dispatch: false }
  );

  autoLogin$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.autoLogin),
      map(() => {
        const token = localStorage.getItem('token');
        if (!token) {
          return { type: 'DUMMY' };
        } else {
          return handleAuthentication(token, false);
        }
      })
    )
  );

  logOut$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.logout),
      tap(() => {
        localStorage.removeItem('token');
        this.store.dispatch(UsersActions.selectUser({selectedUser: null}));
        this.router.navigate(['/login']);
      })
    ), { dispatch: false }
  );

  constructor(
    private http: HttpClient,
    private actions$: Actions,
    private router: Router,
    private store: Store<fromApp.AppState>,
    private flashMessage: FlashMessagesService
  ) { }
}
