import { Action, createReducer, on } from '@ngrx/store';

import * as AuthActions from './auth.actions';

export interface State {
  token: string;
  id: number;
}

export const initialState: State = {
  token: null,
  id: null
};

export function authReducer(authState: State | undefined, authAction: Action) {
  return createReducer(
    initialState,
    on(AuthActions.authenticateSuccess,
      (state, action) => ({
        ...state,
        token: action.token,
        id: action.id
      })
    ),
    on(AuthActions.logout,
      (state, action) => ({
        ...state,
        token: null,
        id: null
      }))
  )(authState, authAction);
}
