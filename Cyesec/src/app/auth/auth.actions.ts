import { createAction, props } from '@ngrx/store';

export const register = createAction(
  '[Auth] Register',
  props<{ email: string, password: string }>()
);

export const login = createAction(
  '[Auth] Login',
  props<{ email: string, password: string }>()
);

export const authenticateSuccess = createAction(
  '[Auth] Authenticate Success',
  props<{ token: string, redirect: boolean, id?: number }>()
);

export const authenticateFail = createAction(
  '[Auth] Authenticate Fail',
  props<{ errorMessage: string }>()
);

export const autoLogin = createAction('[Auth] Auto Login');

export const logout = createAction('[Auth] Logout');
